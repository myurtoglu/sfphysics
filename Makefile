uname_S := $(shell sh -c 'uname -s 2>/dev/null || echo not')

CXX = clang++
CPPFLAGS = -std=c++11 -Wall -Wextra -Wfloat-equal -pedantic -g

SFGUI = -lsfgui
BOX2D = -lBox2D

ifeq ($(uname_S), Darwin)
	SF_SYSTEM = -framework sfml-system
	SF_WINDOW = -framework sfml-window
	SF_GRAPHICS = -framework sfml-graphics
	SF_AUDIO = -framework sfml-audio
	SF_NETWORK = -framework sfml-network
	FOUNDATION = -framework Foundation
	LDFLAGS = $(SF_SYSTEM) $(SF_WINDOW) $(SF_GRAPHICS) $(SF_AUDIO) \
		  	  $(SF_NETWORK) $(FOUNDATION) $(SFGUI) $(BOX2D)
endif

ifeq ($(uname_S), Linux)
	LDFLAGS = -lsfml-system -lsfml-window -lsfml-graphics -lsfml-audio \
			  -lsfml-network -lobjc -lgnustep-base \
			  $(SFGUI) $(BOX2D)
	INCLUDES = -I/usr/include/GNUstep
endif

OBJECTS = main.o Game.o World.o Gui.o ResourcePath.o FixtureShape.o \
		  Shape.o RectShape.o CircShape.o

.PHONY: clean clobber

main.exe: $(OBJECTS)
	$(CXX) $(OBJECTS) -o $@ $(LDFLAGS)

main.o: main.cpp Game.hpp
	$(CXX) $(CPPFLAGS) -c $<

Game.o: Game.cpp Game.hpp World.hpp ResourcePath.hpp BoxSfmlHelpers.hpp \
		Gui.hpp
	$(CXX) $(CPPFLAGS) -c $<

CircShape.o: CircShape.cpp CircShape.hpp BoxSfmlHelpers.hpp Shape.hpp
	$(CXX) $(CPPFLAGS) -c $<

RectShape.o: RectShape.cpp RectShape.hpp BoxSfmlHelpers.hpp Shape.hpp
	$(CXX) $(CPPFLAGS) -c $<

FixtureShape.o: FixtureShape.cpp FixtureShape.hpp BoxSfmlHelpers.hpp Shape.hpp
	$(CXX) $(CPPFLAGS) -c $<

Shape.o: Shape.cpp Shape.hpp
	$(CXX) $(CPPFLAGS) -c $<

Gui.o: Gui.cpp Gui.hpp World.hpp
	$(CXX) $(CPPFLAGS) -c $<

World.o: World.cpp World.hpp RectShape.hpp CircShape.hpp \
		 FixtureShape.hpp ResourcePath.hpp BoxSfmlHelpers.hpp
	$(CXX) $(CPPFLAGS) -c $<

ResourcePath.o: ResourcePath.mm ResourcePath.hpp
	$(CXX) $(INCLUDES) $(CPPFLAGS) -c $<

clean:
	rm -rf $(OBJECTS)

clobber: clean
	rm -rf main.exe

